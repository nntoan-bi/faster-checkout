<?php
/**
 * Copyright © 2017 Balance Internet Pty., Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Balance\FasterCheckout\Plugin\Checkout\Block;

use Magento\Checkout\Block\Checkout\LayoutProcessor as CheckoutLayoutProcessor;
use Magento\Payment\Model\Config as PaymentConfig;
use Magento\Payment\Model\Method\Factory as PaymentMethodFactory;
use Balance\Core\Helper\Data as BalanceHelper;

/**
 * Plugin LayoutProcessor
 * - This plugin will remove all unused payment methods from JS Layout to reduce the load time in checkout page
 *
 * @package Balance\FasterCheckout\Plugin\Checkout\Block
 * @author  Toan Nguyen <toan.nguyen@balanceinternet.com.au>
 */
class LayoutProcessor
{
    /**
     * @var PaymentConfig
     */
    protected $paymentConfig;

    /**
     * @var PaymentMethodFactory
     */
    protected $paymentMethodFactory;

    /**
     * @var BalanceHelper
     */
    protected $helper;

    /**
     * LayoutProcessor constructor.
     *
     * @param PaymentMethodFactory $paymentMethodFactory
     * @param BalanceHelper        $balanceHelper
     */
    public function __construct(
        PaymentMethodFactory $paymentMethodFactory,
        BalanceHelper $balanceHelper
    ) {
        $this->paymentMethodFactory = $paymentMethodFactory;
        $this->helper = $balanceHelper;
    }

    /**
     * Remove unused payment methods from JS Layout
     *
     * @param CheckoutLayoutProcessor $subject
     * @param array                   $jsLayout
     *
     * @return array|mixed
     */
    public function beforeProcess(CheckoutLayoutProcessor $subject, array $jsLayout)
    {
        $jsLayout = $this->removeUnusedPaymentMethods($jsLayout);

        return $jsLayout;
    }

    /**
     * Remove unused payment methods
     *
     * @param array $jsLayout
     *
     * @return mixed
     */
    protected function removeUnusedPaymentMethods($jsLayout)
    {
        if (isset($jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['renders']['children'])) {
            $paymentRenders = $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['renders']['children'];
            $paymentRenders = $this->removePaymentRenders($paymentRenders);

            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['renders']['children'] = $paymentRenders;
        }

        if (isset($jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children'])) {
            $paymentListChildren = $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children'];
            $paymentListChildren = $this->removePaymentListChildren($paymentListChildren);

            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children'] = $paymentListChildren;
        }

        return [$jsLayout];
    }

    /**
     * Remove inactive methods from payment renders to prevent billion of uncessary JS requests
     *
     * @param array $paymentRenders Payment renders
     *
     * @return mixed
     */
    protected function removePaymentRenders($paymentRenders)
    {
        foreach ($this->helper->getConfig('payment') as $code => $data) {
            if (isset($data['active']) && (bool)$data['active'] === false) {
                if (isset($paymentRenders[$code])) {
                    unset($paymentRenders[$code]);
                }

                if (isset($paymentRenders[$code . '-payments'])) {
                    unset($paymentRenders[$code . '-payments']);
                }

                if (isset($paymentRenders['offline-payments']['methods'][$code])) {
                    unset($paymentRenders['offline-payments']['methods'][$code]);
                }

                if ($code === 'authorizenet_directpost') {
                    unset($paymentRenders['authorizenet']);
                }

                if ($code === 'afterpaypayovertime') {
                    unset($paymentRenders['afterpay-payments']);
                }
            }
        }

        return $paymentRenders;
    }

    /**
     * Remove inactive payment methods from payment list to cut-down JSON size!!!
     *
     * @param array $paymentListChildren Children elements in [payment-list]
     *
     * @return mixed
     */
    protected function removePaymentListChildren($paymentListChildren)
    {
        foreach ($this->helper->getConfig('payment') as $code => $data) {
            if (isset($data['active']) && (bool)$data['active'] === false) {
                if (isset($paymentListChildren[$code . '-form'])) {
                    unset($paymentListChildren[$code . '-form']);
                }
            }
        }

        return $paymentListChildren;
    }
}